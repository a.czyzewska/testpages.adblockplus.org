template = testcase
title = Generic Hide
description = Check that a filter using the $generichide exception filter option is working as expected.

<section class="testcase-panel">
  {{ heading("Basic usage") }}
  <p>Test that a filter using the $generichide filter option whitelists only generic hiding filters.</p>
  <div class="testcase-area">
    <div class="ex-gh-specific" data-expectedresult="fail">Hide Target</div>
    <div class="testcase-expected-view">Exception target</div>
    <div class="ex-gh-generic" data-expectedresult="pass">Exception target</div>
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1 and filter #2.</li>
    <li>Refresh page.</li>
    <li>Both the red element and the green element containing the exception target should be blocked.</li>
    <li>Add filter #3.</li>
    <li>The red element should still be blocked but the green element containing the exception target should become unblocked.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>##.ex-gh-generic</pre></li>
    <li><pre>{{ site_url|domain }}##.ex-gh-specific</pre></li>
    <li><pre>@@||{{ site_url|strip_proto }}/en/exceptions/generichide$generichide</pre></li>
  </ul>
</section>
