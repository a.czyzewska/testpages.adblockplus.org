template = testcase
title = Image
description = Check that a filter using the $image filter option is working as expected.

<section class="testcase-panel">
  {{ heading("Static") }}
  <p>Test that a filter using the $image filter option works on a static target.</p>
  <div class="testcase-area">
    <img src="/testfiles/image/static/static.png" data-expectedresult="fail">
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red image should be blocked and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>||{{ site_url|strip_proto }}/testfiles/image/static/$image</pre></li>
  </ul>
</section>

<section class="testcase-panel">
  {{ heading("Dynamic") }}
  <p>Test that a filter using the $image filter option works on a dynamic target.</p>
  <div id="image-dynamic" class="testcase-area">
    <div class="testcase-examplecontent">Example Content</div>
  </div>
  <h3>Steps</h3>
  <ul class="testcase-steps">
    <li>Add filter #1.</li>
    <li>Refresh page.</li>
    <li>The red image should be blocked and the space collapsed.</li>
  </ul>
  <h3>Filters</h3>
  <ul class="testcase-filters">
    <li><pre>||{{ site_url|strip_proto }}/testfiles/image/dynamic/$image</pre></li>
  </ul>
</section>

<script>
  "use strict";

  let image = document.createElement("img");
  image.src = "/testfiles/image/dynamic/dynamic.png";
  image.setAttribute("data-expectedresult", "fail");

  let area = document.getElementById("image-dynamic");
  area.appendChild(image);
</script>
